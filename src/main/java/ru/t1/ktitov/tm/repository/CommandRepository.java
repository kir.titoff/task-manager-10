package ru.t1.ktitov.tm.repository;

import ru.t1.ktitov.tm.api.ICommandRepository;
import ru.t1.ktitov.tm.constant.ArgumentConst;
import ru.t1.ktitov.tm.constant.TerminalConst;
import ru.t1.ktitov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public final static Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects"
    );
    public final static Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Show all projects"
    );
    public final static Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project"
    );
    public final static Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks"
    );
    public final static Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show all tasks"
    );
    public final static Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task"
    );
    private final static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version"
    );
    private final static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info"
    );
    private final static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show application commands"
    );
    private final static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Exit"
    );
    private final static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show available memory"
    );
    private final static Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show available commands"
    );
    private final static Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show available arguments"
    );
    private final static Command[] TERMINAL_COMMANDS = new Command[]{
            VERSION, ABOUT, HELP, INFO,
            COMMANDS, ARGUMENTS, EXIT,
            PROJECT_CLEAR, PROJECT_LIST, PROJECT_CREATE,
            TASK_CLEAR, TASK_LIST, TASK_CREATE
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
