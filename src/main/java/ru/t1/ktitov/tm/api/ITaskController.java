package ru.t1.ktitov.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

}
