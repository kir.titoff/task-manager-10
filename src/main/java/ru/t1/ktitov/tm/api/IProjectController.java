package ru.t1.ktitov.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

}
