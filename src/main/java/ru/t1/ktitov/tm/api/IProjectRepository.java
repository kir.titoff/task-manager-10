package ru.t1.ktitov.tm.api;

import ru.t1.ktitov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    List<Project> findAll();

}
